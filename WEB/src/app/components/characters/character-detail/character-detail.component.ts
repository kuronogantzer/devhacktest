import { Component, OnInit } from '@angular/core';
import { RmServiceService } from '../../../services/rm-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-character-detail',
    templateUrl: './character-detail.component.html',
    styles: []
})
export class CharacterDetailComponent implements OnInit {

    character;

    constructor(private activatedRoute: ActivatedRoute,
        private _rmServiceService:RmServiceService) { }

        ngOnInit() {
            this.activatedRoute.params.subscribe( params => {
                this._rmServiceService.GetCharacterById(params['id'])
                    .subscribe( data => {
                        console.log(data);
                        this.character = data;
                    })
            })
        }

    }
