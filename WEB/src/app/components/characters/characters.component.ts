import { Component, OnInit } from '@angular/core';
import { RmServiceService } from '../../services/rm-service.service';

@Component({
    selector: 'app-characters',
    templateUrl: './characters.component.html',
    styles: []
})
export class CharactersComponent implements OnInit {

    characters = [];
    currentCharacters = [];
    private nextPage;
    private previousPage;
    showPrev: boolean;
    showNext: boolean;
    grid;
    startIndex: number;
    finishIndex: number;

    constructor(private _rmServiceService: RmServiceService) { }

    ngOnInit() {
        this._rmServiceService.GetCharacters()
        .subscribe( (data:any) => {
            console.log(data);
            this.nextPage = data.info.next;
            this.previousPage = data.info.prev;
            this.characters = data.results;
            this.startIndex = 0;
            this.finishIndex = 3
            this.currentCharacters = this.characters.slice(this.startIndex, this.finishIndex);
        });
    }

    getNextPage() {
        this.showPrev = true;
        if(!this.grid)
        {
            this.startIndex += 3;
            this.finishIndex += 3;
            let nextCharacters = this.characters.slice(this.startIndex, this.finishIndex);
            if (nextCharacters.length > 0) {
                this.currentCharacters = nextCharacters;
            }
            else {
                this._rmServiceService.GetPage(this.nextPage)
                .subscribe( (data:any) => {
                    console.log(data);
                    this.nextPage = data.info.next;
                    this.previousPage = data.info.prev;
                    this.characters = data.results;
                    this.startIndex = 0;
                    this.finishIndex = 3
                    this.currentCharacters = this.characters.slice(this.startIndex, this.finishIndex);
                });
            }
            if (!this.nextPage || this.nextPage === "") {
                this.showNext = false;
            }
        }
        else {
            this._rmServiceService.GetPage(this.nextPage)
            .subscribe( (data:any) => {
                console.log(data);
                this.nextPage = data.info.next;
                this.previousPage = data.info.prev;
                this.characters = data.results;
                this.startIndex = 0;
                this.finishIndex = 3
                this.currentCharacters = this.characters.slice(this.startIndex, this.finishIndex);
            });
        }
    }

    getPrevPage() {
        this.showNext = true;
        if(!this.grid)
        {
            this.startIndex -= 3;
            this.finishIndex -= 3;
            let prevCharacters = this.characters.slice(this.startIndex, this.finishIndex);
            if (prevCharacters.length > 0) {
                this.currentCharacters = prevCharacters;
            }
            else {
                this._rmServiceService.GetPage(this.previousPage)
                    .subscribe( (data:any) => {
                        console.log(data);
                        this.nextPage = data.info.next;
                        this.previousPage = data.info.prev;
                        this.characters = data.results;
                        this.startIndex = 0;
                        this.finishIndex = 3
                        this.currentCharacters = this.characters.slice(this.startIndex, this.finishIndex);
                    });
            }
            if (!this.previousPage || this.previousPage === "") {
                this.showPrev = false;
            }
        }
        else {
            this._rmServiceService.GetPage(this.previousPage)
            .subscribe( (data:any) => {
                console.log(data);
                this.nextPage = data.info.next;
                this.previousPage = data.info.prev;
                this.characters = data.results;
            });
        }
    }
}
