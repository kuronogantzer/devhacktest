import { TestBed } from '@angular/core/testing';

import { RmServiceService } from './rm-service.service';

describe('RmServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RmServiceService = TestBed.get(RmServiceService);
    expect(service).toBeTruthy();
  });
});
