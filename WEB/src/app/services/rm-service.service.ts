import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RmServiceService {

    BaseUrl = 'https://rickandmortyapi.com/api/'

    constructor(private _http: HttpClient ) { }

    GetCharacters() {
        return this._http.get(this.BaseUrl + "character");
    }

    GetCharacterById(characterId: number) {
        return this._http.get(this.BaseUrl + "character/" + characterId.toString());
    }

    GetPage(url:string) {
        return this._http.get(url);
    }
}
