# DevHackTest

## ¿Para qué tipo de proyectos recomienda usar Angular(2+)?

Para aplicaciones web que impliquen varios componentes interactuando entre sí y/o con una estructura relativamente compleja.

## ¿Cuando se debe usar Componentes? Defina un Componente en un .ts

Cuando se requiera agrupar un conjunto de controles que cumplan una función concreta como parte de un proceso mas grande.
```
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styles: []
})
export class TestComponent implements OnInit {

}
```
## ¿Cuando se debe usar Directivas? Defina una Directiva en un .ts

Cuando se requiera que ciertos elementos html tengan un comportamiento definido con base a aunos parametros de entrada
```
import { Directive, Input, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appTest]'
})

export class TestDirective {

  constructor(private eleRef : ElementRef) {
  }
  
}
```
## ¿Cuál es la diferencia entre un Componente y una Directiva?

Un componente en una agrupacion de elementos html que cumplen una funcionalidad concreta dentro de la aplicación y generalmente con una sola ubicación.
Una directiva define un comportamiento para cualquier elemento html y puede estar presente en cualquier parte donde se requiera y tantas veces como se necesite.

## ¿Para que se utilizan los Servicios en Angular? Defina un Servicio en un .ts

Se utilizan para proveer información a los componenetes principalmente de fuentes externas a la aplicación.
```
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TestService {

    private API_URL : string = "http://www.testapi.com:3000/api/v1/";

    constructor(private http: HttpClient) {  }

    GetOrders() {
        return this.http.get(this.API_URL + "orders");
    }
}
```
## ¿Para que se utilizan los Pipes en Angular? Defina un Pipe en un .ts

Para modificar la forma en que se muestran los datos al usuario en pantalla.
```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'test'
})
export class TestPipe implements PipeTransform {
	
	transform(value: any, args?: any): any {
		return value;
	}
}
```
## ¿Que es una Promesa? Defina la estructura general de una promesa imprimendo en console.log() si hay respuesta existosa y en console.error() si no hay respuesta.

Una promesa es un método que ejecuta un proceso asíncrono y hace el llamado a una función si se resuelve de forma exitosa o a otra si ocurre algún error.
```
let promesa = new Promise( (resolve, reject) => {
	try {
		asyncMethod().subscribe( data => {
				resolve();
			});
	}
	catch(err) {
		reject(err);
	}
});

promesa.then(function() {
		console.log("Respuesta exitosa");
	},
	function(err) {
		console.error("No hay respuesta: " + err.toString());
	}
);
```
## ¿Que es un Observable? Defina un metodo que se llame getInfo() e imprima en consola el llamado a https://fakeApi/fakeEndpoint usando una Observable.

Es un objeto que permite ejecutar una función definida al finalizar la ejecución de un proceso asíncrono con su resultado como parámetro de entrada.
```
import { HttpClient } from '@angular/common/http';

let http: HttpClient = new HttpClient();

getInfo() {
	this.http.get('https://fakeApi/fakeEndpoint').subscribe( data => {
		console.log(data);
	});
}
```
## ¿Como funciona el sistema de enrutamiento en Angular? Defina un enrutamiento /dashboard que llame al componente dashboardComponent, a /users que llame al componente usersComponent y por defecto en caso de que la ruta sea desconocida redirija a /

Angular permite asociar una estructura de ruta URL dentro de la aplicación a un componente de modo que si el usuario navega a la ruta angular renderiza el componente asociado.
```
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { UsersComponent } from './component/users/users.component';
import { HomeComponent } from './component/home/home.component';

const APPROUTES: Routes = [
    { path: '/', component: HomeComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'users', component: UsersComponent },
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APPROUTES);
```
## ¿Qué es un Módulo y cual es la recomendación para su uso?

Un módulo es una clase que utiliza un decorador que le permite definir la estructura y organización de componentes, servicios, módulos, pipes, directivas y demás dentro de un mismo contexto funcional de la aplicación.
En aplicaciones de gran tamaño es recomendable separar la aplicación en una jerarquía de módulos de modo que la aplicación solamente consuma los recusos necesarios para los elementos correspondientes al módulo utilizado en ese momento.

## ¿En qué proyectos recomiendas utilizar Promesas y en cual Observables?

Las promesas son muy utiles en proyectos donde la aplicación realiza una gran cantidad de procesos asíncronos suceptibles a fallos que retornan el error en forma de excepción.
Los observables son muy recomendados para proyectos con muchas peticiones a serivios http ya que tanto la respuesta exitosa como fallida retornan en el mismo objeto.

## ¿Conoces sobre el ciclo de vida de los Hooks?, ¿Cuáles son los principales que recuerdes y el orden?

Si, es el ciclo de vida de los componentes de Angular. Generalmente se utilizan el OnChanges, que se ejecuta cuando angular prepara el cambio a un nuevo componente, luego el OnInit
que se ejecuta al inicializar el componente, luego el DoCheck que se ejecuta cuando ocurre algún cambio en cualquier parte del componente y finalmente el OnDestroy que se ejecuta justo
antes de que el componente desaparezca.

## ¿Conoce de Event Emitters?, ¿para qué los usó?

Si, los uso mucho para invocar una acción en un componente desde otros componentes hijos, de modo que se actualice constantemente.

## ¿Como funciona el Change Detection en Angular?

El change detection se ejecuta siempre que se modifica la data de la aplicación en general. Al iniciar la aplicación y cada componente, se almacena el estado actual de la data,
luego, cuando ocurre algún cambio de data, el change detection compara la nueva data en memoria y actualiza la vista de acuerdo a los nuevos cambios ocurridos.

## ¿Que es Redux?

Es un patron para el manejo de la información de la aplicación que consiste en almacenar un objeto en memoria con el estado de la información en determinado momento, de modo que no sea
necesario consultar el servidor constantemente por información que no ha cambiado.

## ¿Has usado Redux con Angular, que librerias?

No, no me he visto en la necesidad de usar dicho patrón.

## ¿Que es JIT y AOT en Angular y cuando lo aplicas?

JIT es la forma en que normalmente se ejecutan las aplicaciones de cualquier framework javascript, con un "compilado" en tiempo de ejecución.
AOT no lo conocía, por lo tanto no lo he utilizado. Sin embargo entiendo que es una técnica que permite generar una aplicación con alguna porción de
código ya precompilado, de modo que el navegador tarde menos tiempo en cargar la aplicación.
